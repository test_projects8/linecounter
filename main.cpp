#include <iostream>
#include <filesystem>
#include <numeric>
#include <vector>
#include <fstream>
#include <future>
#include <string>

namespace fSys = std::filesystem;

size_t readAndCount(const fSys::directory_entry &dEntry)
{
	const auto& path = dEntry.path();
	std::fstream file(path, std::fstream::in);
	if (!file.is_open())
	{
		std::cout << "No access to file " << path << std::endl;
		return 0;
	}
	size_t counter {};
	std::string buffer;
	for (; std::getline(file, buffer); ++counter);
	file.close();
    
	return counter;
}

int main(int argc, char **argv)
{
	if (argc == 1)
	{
		std::cout << "URL not entered. Program stopped." << std::endl;
		return -1;
	}
    
	fSys::path dir {argv[1]};
	if (!fSys::exists(dir))
	{
		std::cout << "Enter correct URL. Program stopped." << std::endl;
		return -1;
	}

	std::vector<std::future<size_t>> vectorParallel;
	for (const auto &entry: fSys::recursive_directory_iterator {dir})
        if (entry.is_regular_file() && entry.path().extension() == ".txt") 
        {
            try 
            {
                vectorParallel.emplace_back(std::async(std::launch::async, readAndCount, entry));
            }
            catch (const std::exception& ex)
            {
                std::cout << "Exception: " << ex.what() << std::endl; 
            }
        }

	int result = std::accumulate(vectorParallel.begin(), vectorParallel.end(), 0, [](const size_t &counter, std::future<size_t> &ft){
		return counter + ft.get();
	});

	std::cout << "Line counter = " << result << std::endl;

	return 0;
}
